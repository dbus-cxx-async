/*
 *
 *  D-Bus++ - C++ bindings for D-Bus
 *
 *  Copyright (C) 2005-2009  Paolo Durante <shackan@gmail.com>
 *
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <dbus-c++/eventloop-integration.h>
#include <dbus-c++/debug.h>

#include <sys/poll.h>

#include <dbus/dbus.h>

DBus::DefaultDispatcher DBus::DefaultDispatcher::instance;

using namespace DBus;

DefaultTimeout::DefaultTimeout(Timeout::Internal *ti, DefaultDispatcher *bd)
: Timeout(ti), SimpleTimeout(Timeout::interval(), true, bd)
{
	SimpleTimeout::enabled(Timeout::enabled());
}

void DefaultTimeout::toggle()
{
	debug_log("timeout %p toggled (%s)", this, Timeout::enabled() ? "on":"off");

	SimpleTimeout::enabled(Timeout::enabled());
}

DefaultWatch::DefaultWatch(Watch::Internal *wi, DefaultDispatcher *bd)
: Watch(wi), SimpleWatch(Watch::descriptor(), 0, bd)
{
	int flags = POLLHUP | POLLERR;

	if (Watch::flags() & DBUS_WATCH_READABLE)
		flags |= POLLIN;
	if (Watch::flags() & DBUS_WATCH_WRITABLE)
		flags |= POLLOUT;

	SimpleWatch::flags(flags);
	SimpleWatch::enabled(Watch::enabled());
}

void DefaultWatch::toggle()
{
	debug_log("watch %p toggled (%s)", this, Watch::enabled() ? "on":"off");

	SimpleWatch::enabled(Watch::enabled());
}

void DefaultDispatcher::enter()
{
	debug_log("entering dispatcher %p", this);

	start_iteration();

	while (_running)
	{
		do_iteration();
	}

	debug_log("leaving dispatcher %p", this);
}

void DefaultDispatcher::leave()
{
	_running = false;
}

void DefaultDispatcher::start_iteration()
{
   _running = true;
}

void DefaultDispatcher::do_iteration()
{
	dispatch_pending();
	dispatch();
}

Timeout *DefaultDispatcher::add_timeout(Timeout::Internal *ti)
{
	DefaultTimeout *bt = new DefaultTimeout(ti, this);

	bt->expired = new CallbackNoReturn<DefaultDispatcher, SimpleTimeout &>(this, &DefaultDispatcher::timeout_expired);
	bt->data(bt);

	debug_log("added timeout %p (%s) interval=%d",
		bt, ((Timeout *)bt)->enabled() ? "on":"off", ((Timeout *)bt)->interval());

	return bt;
}

void DefaultDispatcher::rem_timeout(Timeout *t)
{
	delete t;

	debug_log("removed timeout %p", t);
}

Watch *DefaultDispatcher::add_watch(Watch::Internal *wi)
{
	DefaultWatch *bw = new DefaultWatch(wi, this);

	bw->ready = new CallbackNoReturn<DefaultDispatcher, SimpleWatch &>(this, &DefaultDispatcher::watch_ready);
	bw->data(bw);

	debug_log("added watch %p (%s) fd=%d flags=%d",
		bw, ((Watch *)bw)->enabled() ? "on":"off", ((Watch *)bw)->descriptor(), ((Watch *)bw)->flags());

	return bw;
}

void DefaultDispatcher::rem_watch(Watch *w)
{
	delete w;

	debug_log("removed watch %p", w);
}

void DefaultDispatcher::timeout_expired(SimpleTimeout &et)
{
	debug_log("timeout %p expired", &et);

	DefaultTimeout *timeout = reinterpret_cast<DefaultTimeout *>(et.data());

	timeout->handle();
}

void DefaultDispatcher::watch_ready(SimpleWatch &ew)
{
	DefaultWatch *watch = reinterpret_cast<DefaultWatch *>(ew.data());

	debug_log("watch %p ready, flags=%d state=%d",
		watch, ((Watch *)watch)->flags(), watch->state()
	);

	int flags = 0;

	if (watch->state() & POLLIN)
		flags |= DBUS_WATCH_READABLE;
	if (watch->state() & POLLOUT)
		flags |= DBUS_WATCH_WRITABLE;
	if (watch->state() & POLLHUP)
		flags |= DBUS_WATCH_HANGUP;
	if (watch->state() & POLLERR)
		flags |= DBUS_WATCH_ERROR;

	watch->handle(flags);
}

