/*
 *
 *  D-Bus++ - C++ bindings for D-Bus
 *
 *  Copyright (C) 2005-2009  Paolo Durante <shackan@gmail.com>
 *
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <dbus-c++/debug.h>
#include <dbus-c++/object.h>
#include "internalerror.h"

#include <cstring>
#include <map>
#include <dbus/dbus.h>

#include "message_p.h"
#include "server_p.h"
#include "connection_p.h"

using namespace DBus;

Object::Object(Connection &conn, const Path &path, const char *service)
: _conn(conn), _path(path), _service(service ? service : "")
{
}

Object::~Object()
{
}

struct ObjectAdaptor::Private
{
	static void unregister_function_stub(DBusConnection *, void *);
	static DBusHandlerResult message_function_stub(DBusConnection *, DBusMessage *, void *);
};

static DBusObjectPathVTable _vtable =
{
	ObjectAdaptor::Private::unregister_function_stub,
	ObjectAdaptor::Private::message_function_stub,
	NULL, NULL, NULL, NULL
};

void ObjectAdaptor::Private::unregister_function_stub(DBusConnection *conn, void *data)
{
 	//TODO: what do we have to do here ?
}

DBusHandlerResult ObjectAdaptor::Private::message_function_stub(DBusConnection *, DBusMessage *dmsg, void *data)
{
	ObjectAdaptor *o = static_cast<ObjectAdaptor *>(data);

	if (o)
	{
		Message msg(new Message::Private(dmsg));

		debug_log("in object %s", o->path().c_str());
		debug_log(" got message #%d from %s to %s",
			msg.serial(),
			msg.sender(),
			msg.destination()
		);

		return o->handle_message(msg)
			? DBUS_HANDLER_RESULT_HANDLED
			: DBUS_HANDLER_RESULT_NOT_YET_HANDLED;
	}
	else
	{
		return DBUS_HANDLER_RESULT_NOT_YET_HANDLED;
	}
}

typedef std::map<Path, ObjectAdaptor *> ObjectAdaptorTable;
static ObjectAdaptorTable _adaptor_table;

ObjectAdaptor *ObjectAdaptor::from_path(const Path &path)
{
	ObjectAdaptorTable::iterator ati = _adaptor_table.find(path);

	if (ati != _adaptor_table.end())
		return ati->second;

	return NULL;
}

ObjectAdaptorPList ObjectAdaptor::from_path_prefix(const std::string &prefix)
{
	ObjectAdaptorPList ali;

	ObjectAdaptorTable::iterator ati = _adaptor_table.begin();

	std::size_t plen = prefix.length();

	while (ati != _adaptor_table.end())
	{
		if (!std::strncmp(ati->second->path().c_str(), prefix.c_str(), plen))
			ali.push_back(ati->second);

		++ati;
	}

	return ali;
}

ObjectPathList ObjectAdaptor::child_nodes_from_prefix(const std::string &prefix)
{
	ObjectPathList ali;

	ObjectAdaptorTable::iterator ati = _adaptor_table.begin();

	std::size_t plen = prefix.length();

	while (ati != _adaptor_table.end())
	{
	  if (!std::strncmp(ati->second->path().c_str(), prefix.c_str(), plen))
		{
				std::string p = ati->second->path().substr(plen);
				p = p.substr(0,p.find('/'));
				ali.push_back(p);
		}
		++ati;
	}

	ali.sort();
	ali.unique();

	return ali;
}

ObjectAdaptor::ObjectAdaptor(Connection &conn, const Path &path)
: Object(conn, path, conn.unique_name())
{
	register_obj();
}

ObjectAdaptor::~ObjectAdaptor()
{
	unregister_obj();
}

void ObjectAdaptor::register_obj()
{
	debug_log("registering local object %s", path().c_str());

	if (!dbus_connection_register_object_path(conn()._pvt->conn, path().c_str(), &_vtable, this))
	{
 		throw ErrorNoMemory("unable to register object path");
	}

	_adaptor_table[path()] = this;
}

void ObjectAdaptor::unregister_obj()
{
	_adaptor_table.erase(path());

	debug_log("unregistering local object %s", path().c_str());

	dbus_connection_unregister_object_path(conn()._pvt->conn, path().c_str());
}

void ObjectAdaptor::_emit_signal(SignalMessage &sig)
{
	sig.path(path().c_str());

	conn().send(sig);
}

bool ObjectAdaptor::handle_message(const Message &msg)
{
	if (msg.is_call())
	{
		const CallMessage &cmsg = reinterpret_cast<const CallMessage &>(msg);
		const char *member = cmsg.member();
		const char *interface = cmsg.interface();

		debug_log(" invoking method %s.%s",
		         interface ? interface : "<no_interface>",
		         member ? member : "<no_member>");

		InterfaceAdaptor *ii = 0;
		
		// If an interface name was not specified then ...
		if ( 0 == interface )
		{
		   // According to the D-Bus spec, an interface with a matching method
		   // name should be used if no interface name is specified. We'll search
		   // for the first matching interface with this member.
		   ii = find_interface_with_method(member ? member : "");   
		}
		else
		{
		   ii = find_interface(interface);
		}
		
		if (ii)
		{
			try
			{
				Message ret = cmsg; //TODO: temporary workaround
				Message *pret = &ret;
				ii->dispatch_method(cmsg, &pret);

				if (pret) // request returned synchronously
					conn().send(ret);
			}
			catch(Error &e)
			{
				ErrorMessage em(cmsg, e.name(), e.description());
				conn().send(em);
			}
			return true;
		}
	}
	return false;
}

/*
*/

ObjectProxy::ObjectProxy(Connection &conn, const Path &path, const char *service)
   : ProxyBase()
   , CallbackTarget()
   , Object(conn, path, service)

{
	register_obj();
}

ObjectProxy::~ObjectProxy()
{
	unregister_obj();
}

void ObjectProxy::register_obj()
{
	debug_log("registering remote object %s", path().c_str());

	_filtered = new Callback<ObjectProxy, bool, const Message &>(this, &ObjectProxy::handle_message, false);

	conn().add_filter(_filtered);

	InterfaceProxyTable::const_iterator ii = _interfaces.begin();
	while (ii != _interfaces.end())
	{
		std::string im = "type='signal',interface='"+ii->first+"',path='"+path()+"'";
		conn().add_match(im.c_str());
		++ii;
	}
}

void ObjectProxy::unregister_obj()
{
	debug_log("unregistering remote object %s", path().c_str());

	InterfaceProxyTable::const_iterator ii = _interfaces.begin();
	while (ii != _interfaces.end())
	{
		std::string im = "type='signal',interface='"+ii->first+"',path='"+path()+"'";
		conn().remove_match(im.c_str());
		++ii;
	}
	conn().remove_filter(_filtered);
}

Message ObjectProxy::_invoke_method(CallMessage &call, int timeout)
{
	if (call.path() == NULL)
		call.path(path().c_str());

	if (call.destination() == NULL)
		call.destination(service().c_str());

	return conn().send_blocking(call, timeout);
}

PendingCall ObjectProxy::_invoke_method_async(CallMessage &call, const PendingCallSlot &slot, void* data, int timeout)
{
	if (call.path() == NULL)
		call.path(path().c_str());

	if (call.destination() == NULL)
		call.destination(service().c_str());

	return conn().send_async(call, this, slot, data, timeout);
}

bool ObjectProxy::handle_message(const Message &msg)
{
	if (msg.is_signal())
	{
		const SignalMessage &smsg = reinterpret_cast<const SignalMessage &>(msg);
		const char *interface	= smsg.interface();
		const char *member	= smsg.member();
		const char *objpath	= smsg.path();

		if (objpath != path()) return false;

		debug_log("filtered signal %s(in %s) from %s to object %s",
			member ? member : "", interface ? interface : "",
			msg.sender(), objpath);

		InterfaceProxy *ii = find_interface(interface ? interface : "");
		if (ii)
		{
			return ii->dispatch_signal(smsg);
		}
	}
	return false;
}
