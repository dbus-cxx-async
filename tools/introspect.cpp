/*
 *
 *  D-Bus++ - C++ bindings for D-Bus
 *
 *  Copyright (C) 2005-2009  Paolo Durante <shackan@gmail.com>
 *
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#include <dbus-c++/dbus.h>

#include <string>
#include <cstring>
#include <iostream>

int main(int argc, char ** argv)
{
	bool systembus;
	char *path;
	char *service;

	if (argc == 1)
	{
		std::cerr << std::endl << "Usage: " << argv[0] << " [--system] <object_path> [<destination>]" << std::endl << std::endl;
	}
	else
	{
		if (std::strcmp(argv[1], "--system"))
		{
			systembus = false;
			path = argv[1];
			service = argc > 2 ? argv[2] : 0;
		}
		else
		{
			systembus = true;
			path = argv[2];
			service = argc > 3 ? argv[3] : 0;
		}

		DBus::Connection conn = systembus ? DBus::Connection::system_bus() : DBus::Connection::session_bus();

		DBus::IntrospectProxy io(conn, path, service);

		std::cout << io.Introspect();
	}

	return 0;
}
