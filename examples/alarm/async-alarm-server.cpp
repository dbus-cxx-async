#include "async-alarm-adaptor.h"
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <pthread.h>
#include <stdio.h>

static const char *ASYNC_ALARM_NAME = "org.freedesktop.DBus.Examples.Alarm";
static const char *ASYNC_ALARM_PATH = "/org/freedesktop/DBus/Examples/Alarm";

class AsyncAlarm
: public org::freedesktop::DBus::Cxx::Example::AsyncAlarm_adaptor,
  public DBus::IntrospectableAdaptor,
  public DBus::ObjectAdaptor
{
public:

	AsyncAlarm(DBus::Connection &connection)
	: DBus::ObjectAdaptor(connection, ASYNC_ALARM_PATH)
	{}

	struct Sleep_params
	{
		uint8_t seconds;
		Sleep_ctxref ctx;
	};

	void Sleep(const uint8_t &seconds, Sleep_ctxref ctx)
	{
		if (seconds > 25) {
			ctx->send_error("org.freedesktop.DBus.Example.InvalidArg", "Timeout larger than 25 seconds");
			return;
		}

		Sleep_params *params = new Sleep_params;
		params->seconds = seconds;
		params->ctx = ctx;

		pthread_t thread;
		pthread_create(&thread, NULL, sleeping_thread, params);
	}

private:

	static void *sleeping_thread(void *arg)
	{
		Sleep_params *params = static_cast<Sleep_params *>(arg);

		uint32_t pid = params->ctx->callInfo.sender_pid();

		for (uint8_t secs = 10 /*TODO*/; secs > 0; --secs) {
			sleep(1);
			printf("alerting pid %d in %i seconds\n", pid, secs);
			params->ctx->iface.CountDown(pid, secs);
		}

		params->ctx->send_reply(false);
		return NULL;
	}
};

void niam(int sig)
{
	DBus::DefaultDispatcher::instance.leave();
}

int main()
{
	signal(SIGTERM, niam);
	signal(SIGINT, niam);

	DBus::Connection conn = DBus::Connection::session_bus();
	conn.request_name(ASYNC_ALARM_NAME);

	AsyncAlarm server(conn);

	DBus::DefaultDispatcher::instance.enter();

	return 0;
}
