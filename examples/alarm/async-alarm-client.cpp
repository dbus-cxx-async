#include "async-alarm-proxy.h"
#include <stdio.h>
#include <signal.h>

using namespace std;

static const char *ASYNC_ALARM_NAME = "org.freedesktop.DBus.Examples.Alarm";
static const char *ASYNC_ALARM_PATH = "/org/freedesktop/DBus/Examples/Alarm";

class AsyncAlarm
: public org::freedesktop::DBus::Cxx::Example::AsyncAlarm_proxy,
  public DBus::IntrospectableProxy,
  public DBus::ObjectProxy
{
public:

	AsyncAlarm(DBus::Connection &connection)
	: DBus::ObjectProxy(connection, ASYNC_ALARM_PATH, ASYNC_ALARM_NAME)
	{}

	void CountDown(const uint32_t &pid, const uint8_t &seconds)
	{
		printf("global notice from server: will answer pid %d in %i seconds\n", pid, seconds);
	}

	void Sleep_cb(const bool& canceled, AsyncAlarm::CallContext &ret)
	{
		if (canceled)
			printf("call canceled by server at pid %d\n", ret.sender_pid());
		else
			printf("received answer from server at pid %d\n", ret.sender_pid());
	}
};

void niam(int sig)
{
	DBus::DefaultDispatcher::instance.leave();
}

int main()
{
	signal(SIGTERM, niam);
	signal(SIGINT, niam);

	DBus::Connection session = DBus::Connection::session_bus();

	AsyncAlarm alarm(session);

	alarm.Sleep_async(10, &alarm, &AsyncAlarm::Sleep_cb, NULL);

	DBus::DefaultDispatcher::instance.enter();

	return 0;
}
