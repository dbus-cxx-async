#include "propsgs-client.h"
#include <iostream>
#include <signal.h>
//#include <pthread.h>

using namespace org::freedesktop::DBus;

static const char *PROPS_SERVER_NAME = "org.freedesktop.DBus.Examples.Properties";
static const char *PROPS_SERVER_PATH = "/org/freedesktop/DBus/Examples/Properties";

PropsClient::PropsClient(DBus::Connection &connection, const char *path, const char *name)
: DBus::ObjectProxy(connection, path, name)
{
}

void PropsClient::MessageChanged(const std::string& message)
{
	std::cout << "MessageChanged signal, new value: " << message << std::endl;
};

void PropsClient::DataChanged(const double& data)
{
	std::cout << "DataChanged signal, new value:" << data << std::endl;
};

void *test_property_proxy(void * input)
{
	PropsClient *client = static_cast<PropsClient*>(input);

	std::cout << "read property 'Version', value:" << client->Version() << std::endl;

	std::cout << "read property 'Message', value:" << client->Message() << std::endl;
	
	client->Message("message set by property access");
	std::cout << "wrote property 'Message'\n";
	
	std::cout << "read property 'Message', value:" << client->Message() << std::endl;
	
	client->Data(1.1);
	std::cout << "wrote property 'Data'" << std::endl;

	return NULL;
}

void niam(int sig)
{
	DBus::DefaultDispatcher::instance.leave();
	pthread_exit(NULL);

}

int main()
{
	signal(SIGTERM, niam);
	signal(SIGINT, niam);

	DBus::_init_threading();

	DBus::Connection conn = DBus::Connection::session_bus();

	PropsClient client (conn, PROPS_SERVER_PATH, PROPS_SERVER_NAME);

	pthread_t thread;
	pthread_create(&thread, NULL, test_property_proxy, &client);

	DBus::DefaultDispatcher::instance.enter();

	return 0;
}
