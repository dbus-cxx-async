#include "propsgs-server.h"
#include <iostream>
#include <signal.h>

static const char *PROPS_SERVER_NAME = "org.freedesktop.DBus.Examples.Properties";
static const char *PROPS_SERVER_PATH = "/org/freedesktop/DBus/Examples/Properties";

PropsServer::PropsServer(DBus::Connection &connection)
: DBus::ObjectAdaptor(connection, PROPS_SERVER_PATH)
{
	Version = 1;
	Message = "default message";
}

void PropsServer::on_set_property
	(DBus::InterfaceAdaptor &interface, const std::string &property, const DBus::Variant &value)
{
	if (property == "Message")
	{
		std::cout << "'Message' has been changed" << std::endl;

		std::string msg = value;
		this->MessageChanged(msg);
	}
	if (property == "Data")
	{
		std::cout << "'Data' has been changed" << std::endl;

		double data = value;
		this->DataChanged(data);
	}
}

void niam(int sig)
{
	DBus::DefaultDispatcher::instance.leave();
}

int main()
{
	signal(SIGTERM, niam);
	signal(SIGINT, niam);

	DBus::Connection conn = DBus::Connection::session_bus();
	conn.request_name(PROPS_SERVER_NAME);

	PropsServer server(conn);

	DBus::DefaultDispatcher::instance.enter();

	return 0;
}
