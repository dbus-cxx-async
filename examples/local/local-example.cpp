#include "local-adaptor.h"
#include "local-proxy.h"
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <pthread.h>
#include <vector>
#include <iostream>

using namespace std;

static const char *LOCAL_EXAMPLE_NAME = "org.freedesktop.DBus.Cxx.Examples.Local";
static const char *LOCAL_EXAMPLE_PATH = "/org/freedesktop/DBus/Cxx/Examples/Local";

class LocalExampleServer
: public org::freedesktop::DBus::Cxx::Example::Local_adaptor,
  public DBus::IntrospectableAdaptor,
  public DBus::ObjectAdaptor
{
public:

	LocalExampleServer(DBus::Connection &connection)
	: DBus::ObjectAdaptor(connection, LOCAL_EXAMPLE_PATH)
	{}

	struct Sum_params
	{
		int32_t a;
		int32_t b;
		Sum_ctxref ctx;
	};
	
	void Sum(const int32_t &a, const int32_t &b, Sum_ctxref ctx)
	{
		cout << "calculating " << a << " + " << b << endl;

		Sum_params* params = new Sum_params;
		params->a = a;
		params->b = b;
		params->ctx = ctx;

		pthread_t thread;
		pthread_create(&thread, NULL, Sum_thread, params);
	}

	static void *Sum_thread(void *ptr)
	{
		Sum_params* params = static_cast<Sum_params *>(ptr);

		sleep(rand() % 10);

		params->ctx->send_reply(params->a + params->b);
		delete params;
		return NULL;
	}
};

class LocalExampleClient
: public org::freedesktop::DBus::Cxx::Example::Local_proxy,
  public DBus::IntrospectableProxy,
  public DBus::ObjectProxy
{
public:

	LocalExampleClient(DBus::Connection &connection, const char *path, const char *name)
	: DBus::ObjectProxy(connection, path, name)
	{
	}

	void Sum_cb(const int32_t &sum, Local_proxy::CallContext &ret)
	{
		if (ret.error())
		{
			cout << "error: " << ret.error().description() << endl;
			return;
		}
		cout << "the sum is " << sum << endl;
	}

	void Status(const uint32_t &pid)
	{
		cout << pid << endl;
	}
};

void *main_thread(void *ptr)
{
	DBus::Connection client_conn = DBus::Connection::session_bus_private();
	
	LocalExampleClient client(client_conn, LOCAL_EXAMPLE_PATH, LOCAL_EXAMPLE_NAME);

	while (DBus::DefaultDispatcher::instance.running())
	{
		int32_t a = rand() % 100;
		int32_t b = rand() % 100;
		client.Sum_async(a, b, &client, &LocalExampleClient::Sum_cb, NULL);
		sleep(rand() % 10);
	}
	return NULL;
}

void niam(int sig)
{
	DBus::DefaultDispatcher::instance.leave();
}

int main()
{
	signal(SIGTERM, niam);
	signal(SIGINT, niam);

	DBus::Connection server_conn = DBus::Connection::session_bus_private();
	server_conn.request_name(LOCAL_EXAMPLE_NAME);

	LocalExampleServer server(server_conn);

	pthread_t thread;
	pthread_create(&thread, NULL, main_thread, NULL);
	
	DBus::DefaultDispatcher::instance.enter();

	return 0;
}
