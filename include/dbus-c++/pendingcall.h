/*
 *
 *  D-Bus++ - C++ bindings for D-Bus
 *
 *  Copyright (C) 2005-2009  Paolo Durante <shackan@gmail.com>
 *
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */


#ifndef __DBUSXX_PENDING_CALL_H
#define __DBUSXX_PENDING_CALL_H

#include "api.h"
#include "util.h"
#include "message.h"

#include <list>

namespace DBus {

class PendingCall;
class Connection;
class CallInfo;

typedef Slot<void, PendingCall &> PendingCallSlot;

class DBUSXXAPI PendingCall
{
public:

	struct Private;

	PendingCall();

	PendingCall(Private *);
	
	PendingCall(const PendingCall &);

	virtual ~PendingCall();

	PendingCall &operator = (const PendingCall &);
	
	bool operator == (const PendingCall &) const;

	bool completed() const;

	void cancel();

	void block();

	void data(void *);

	void *data() const;

	const PendingCallSlot &slot() const;

private:

	RefPtrI<Private> _pvt;

friend struct Private;
friend class Connection;
friend class CallInfo;
};

typedef std::list<DBus::PendingCall> PendingCallList;
typedef PendingCallList::iterator PendingToken;

class PendingCalls : private PendingCallList, public Synchronized
{
public:

	PendingCalls() {}

	~PendingCalls()
	{
		Guard g(this);

		while (!empty())
			PendingCallList::erase(begin());
	}

	PendingToken insert(DBus::PendingCall &call)
	{
		Guard g(this);

		return PendingCallList::insert(end(), call);
	}

	void erase(const PendingToken& token)
	{
		Guard g(this);

		PendingCallList::erase(token);
	}
};


} /* namespace DBus */

#endif//__DBUSXX_PENDING_CALL_H
