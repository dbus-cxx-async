/*
 *
 *  D-Bus++ - C++ bindings for D-Bus
 *
 *  Copyright (C) 2005-2009  Paolo Durante <shackan@gmail.com>
 *
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */


#ifndef __DBUSXX_ERROR_H
#define __DBUSXX_ERROR_H

#include "api.h"
#include "util.h"

#include <exception>

namespace DBus {

class Message;
class ErrorMessage;
class InternalError;

class DBUSXXAPI Error : public std::exception
{
public:

	Error();

	Error(InternalError &);

	Error(const char *name, const char *description);
	
	Error(Message &);

	~Error() throw();

	const char *what() const throw();

	const char *name() const;

	const char *description() const;

	void set(const ErrorMessage &error);

	void set(const char *name, const char *description);
	// parameters MUST be static strings

	bool is_set() const;

	operator bool() const
	{
		return is_set();
	}

private:

	RefPtrI<InternalError> _int;
};

struct DBUSXXAPI ErrorFailed : public Error
{
	ErrorFailed(const char *description)
	: Error("org.freedesktop.DBus.Error.Failed", description)
	{}
};

struct DBUSXXAPI ErrorNoMemory : public Error
{
	ErrorNoMemory(const char *description)
	: Error("org.freedesktop.DBus.Error.NoMemory", description)
	{}
};

struct DBUSXXAPI ErrorServiceUnknown : public Error
{
	ErrorServiceUnknown(const char *description)
	: Error("org.freedesktop.DBus.Error.ServiceUnknown", description)
	{}
};

struct DBUSXXAPI ErrorNameHasNoOwner : public Error
{
	ErrorNameHasNoOwner(const char *description)
	: Error("org.freedesktop.DBus.Error.NameHasNoOwner", description)
	{}
};

struct DBUSXXAPI ErrorNoReply : public Error
{
	ErrorNoReply(const char *description)
	: Error("org.freedesktop.DBus.Error.NoReply", description)
	{}
};

struct DBUSXXAPI ErrorIOError : public Error
{
	ErrorIOError(const char *description)
	: Error("org.freedesktop.DBus.Error.IOError", description)
	{}
};

struct DBUSXXAPI ErrorBadAddress : public Error
{
	ErrorBadAddress(const char *description)
	: Error("org.freedesktop.DBus.Error.BadAddress", description)
	{}
};

struct DBUSXXAPI ErrorNotSupported : public Error
{
	ErrorNotSupported(const char *description)
	: Error("org.freedesktop.DBus.Error.NotSupported", description)
	{}
};

struct DBUSXXAPI ErrorLimitsExceeded : public Error
{
	ErrorLimitsExceeded(const char *description)
	: Error("org.freedesktop.DBus.Error.LimitsExceeded", description)
	{}
};

struct DBUSXXAPI ErrorAccessDenied : public Error
{
	ErrorAccessDenied(const char *description)
	: Error("org.freedesktop.DBus.Error.AccessDenied", description)
	{}
};

struct DBUSXXAPI ErrorAuthFailed : public Error
{
	ErrorAuthFailed(const char *description)
	: Error("org.freedesktop.DBus.Error.AuthFailed", description)
	{}
};

struct DBUSXXAPI ErrorNoServer : public Error
{
	ErrorNoServer(const char *description)
	: Error("org.freedesktop.DBus.Error.NoServer", description)
	{}
};

struct DBUSXXAPI ErrorTimeout : public Error
{
	ErrorTimeout(const char *description)
	: Error("org.freedesktop.DBus.Error.Timeout", description)
	{}
};

struct DBUSXXAPI ErrorNoNetwork : public Error
{
	ErrorNoNetwork(const char *description)
	: Error("org.freedesktop.DBus.Error.NoNetwork", description)
	{}
};

struct DBUSXXAPI ErrorAddressInUse : public Error
{
	ErrorAddressInUse(const char *description)
	: Error("org.freedesktop.DBus.Error.AddressInUse", description)
	{}
};

struct DBUSXXAPI ErrorDisconnected : public Error
{
	ErrorDisconnected(const char *description)
	: Error("org.freedesktop.DBus.Error.Disconnected", description)
	{}
};

struct DBUSXXAPI ErrorInvalidArgs : public Error
{
	ErrorInvalidArgs(const char *description)
	: Error("org.freedesktop.DBus.Error.InvalidArgs", description)
	{}
};

struct DBUSXXAPI ErrorFileNotFound : public Error
{
	ErrorFileNotFound(const char *description)
	: Error("org.freedesktop.DBus.Error.FileNotFound", description)
	{}
};

struct DBUSXXAPI ErrorUnknownMethod : public Error
{
	ErrorUnknownMethod(const char *description)
	: Error("org.freedesktop.DBus.Error.UnknownMethod", description)
	{}
};

struct DBUSXXAPI ErrorTimedOut : public Error
{
	ErrorTimedOut(const char *description)
	: Error("org.freedesktop.DBus.Error.TimedOut", description)
	{}
};

struct DBUSXXAPI ErrorMatchRuleNotFound : public Error
{
	ErrorMatchRuleNotFound(const char *description)
	: Error("org.freedesktop.DBus.Error.MatchRuleNotFound", description)
	{}
};

struct DBUSXXAPI ErrorMatchRuleInvalid : public Error
{
	ErrorMatchRuleInvalid(const char *description)
	: Error("org.freedesktop.DBus.Error.MatchRuleInvalid", description)
	{}
};

struct DBUSXXAPI ErrorSpawnExecFailed : public Error
{
	ErrorSpawnExecFailed(const char *description)
	: Error("org.freedesktop.DBus.Error.Spawn.ExecFailed", description)
	{}
};

struct DBUSXXAPI ErrorSpawnForkFailed : public Error
{
	ErrorSpawnForkFailed(const char *description)
	: Error("org.freedesktop.DBus.Error.Spawn.ForkFailed", description)
	{}
};

struct DBUSXXAPI ErrorSpawnChildExited : public Error
{
	ErrorSpawnChildExited(const char *description)
	: Error("org.freedesktop.DBus.Error.Spawn.ChildExited", description)
	{}
};

struct DBUSXXAPI ErrorSpawnChildSignaled : public Error
{
	ErrorSpawnChildSignaled(const char *description)
	: Error("org.freedesktop.DBus.Error.Spawn.ChildSignaled", description)
	{}
};

struct DBUSXXAPI ErrorSpawnFailed : public Error
{
	ErrorSpawnFailed(const char *description)
	: Error("org.freedesktop.DBus.Error.Spawn.Failed", description)
	{}
};

struct DBUSXXAPI ErrorInvalidSignature : public Error
{
	ErrorInvalidSignature(const char *description)
	: Error("org.freedesktop.DBus.Error.InvalidSignature", description)
	{}
};

struct DBUSXXAPI ErrorUnixProcessIdUnknown : public Error
{
	ErrorUnixProcessIdUnknown(const char *description)
	: Error("org.freedesktop.DBus.Error.UnixProcessIdUnknown", description)
	{}
};

struct DBUSXXAPI ErrorSELinuxSecurityContextUnknown : public Error
{
	ErrorSELinuxSecurityContextUnknown(const char *description)
	: Error("org.freedesktop.DBus.Error.SELinuxSecurityContextUnknown", description)
	{}
};

} /* namespace DBus */

#endif//__DBUSXX_ERROR_H
