/*
 *
 *  D-Bus++ - C++ bindings for D-Bus
 *
 *  Copyright (C) 2005-2009  Paolo Durante <shackan@gmail.com>
 *
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */


#ifndef __DBUSXX_EVENTLOOP_H
#define __DBUSXX_EVENTLOOP_H

#include <pthread.h>
#include <list>
#include <stdint.h>

#include "api.h"
#include "util.h"

namespace DBus {

/*
 * these Simple *classes implement a very simple event loop which
 * is used here as the default main loop, if you want to hook
 * a different one use the Bus *classes in eventloop-integration.h
 * or the Glib::Bus *classes as a reference
 */

class SimpleMainLoop;

class DBUSXXAPI SimpleTimeout
{
public:

	SimpleTimeout(int interval, bool repeat, SimpleMainLoop *);

	virtual ~SimpleTimeout();

	bool enabled(){ return _enabled; }
	void enabled(bool e){ _enabled = e; }

	int interval(){ return _interval; }
	void interval(int i){ _interval = i; }

	bool repeat(){ return _repeat; }
	void repeat(bool r){ _repeat = r; }

	void *data(){ return _data; }
	void data(void *d){ _data = d; }

	Slot<void, SimpleTimeout &> expired;
	
private:

	bool _enabled;

	int _interval;
	bool _repeat;

	double _expiration;

	void *_data;
	
	SimpleMainLoop *_disp;

friend class SimpleMainLoop;
};

typedef std::list< SimpleTimeout *> SimpleTimeouts;

class DBUSXXAPI SimpleWatch
{
public:

	SimpleWatch(int fd, int flags, SimpleMainLoop *);

	virtual ~SimpleWatch();

	bool enabled(){ return _enabled; }
	void enabled(bool e){ _enabled = e; }

	int descriptor(){ return _fd; }

	int flags(){ return _flags; }
	void flags(int f){ _flags = f; }

	int state(){ return _state; }

	void *data(){ return _data; }
	void data(void *d){ _data = d; }

	Slot<void, SimpleWatch &> ready;

private:

	bool _enabled;

	int _fd;
	int _flags;
	int _state;

	void *_data;

	SimpleMainLoop *_disp;

friend class SimpleMainLoop;
};

typedef std::list< SimpleWatch *> SimpleWatches;

class DBUSXXAPI SimpleMutex
{
public:

	SimpleMutex();

	~SimpleMutex();

	void lock();

	void unlock();

private:

	pthread_mutex_t _mutex;
};

class DBUSXXAPI SimpleMainLoop
{
public:

	SimpleMainLoop(int32_t pollTimeout);

	virtual ~SimpleMainLoop();

	virtual void dispatch();

private:

	SimpleMutex _mutex_t;
	SimpleTimeouts _timeouts;

	SimpleMutex _mutex_w;
	SimpleWatches _watches;
	int32_t _pollTimeout;

friend class SimpleTimeout;
friend class SimpleWatch;
};

} /* namespace DBus */

#endif//__DBUSXX_EVENTLOOP_H
