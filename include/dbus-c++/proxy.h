/*
 *
 *  D-Bus++ - C++ bindings for D-Bus
 *
 *  Copyright (C) 2005-2009  Paolo Durante <shackan@gmail.com>
 *
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */


#ifndef __DBUSXX_PROXY_H
#define __DBUSXX_PROXY_H

#include "interface.h"
#include "object.h"

#include <dbus-c++/dbus-proxy.h>

namespace DBus {

class IntrospectProxy
: public DBus::IntrospectableProxy,
  public DBus::ObjectProxy
{
public:

	IntrospectProxy(DBus::Connection &conn, const char *path, const char *service)
	: DBus::ObjectProxy(conn, path, service)
	{}
};

class BusProxy
: public org::freedesktop::DBus_proxy,
  public DBus::IntrospectableProxy,
  public DBus::ObjectProxy
{
public:

	BusProxy(DBus::Connection &connection)
	: DBus::ObjectProxy(connection, "/org/freedesktop/DBus", "org.freedesktop.DBus")
	{}

	void NameOwnerChanged(const std::string& argin0, const std::string& argin1, const std::string& argin2) {}
	void NameLost(const std::string& argin0) {}
	void NameAcquired(const std::string& argin0) {}
};

} /* namespace DBus */

#endif//__DBUSXX_PROXY_H
