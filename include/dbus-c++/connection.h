/*
 *
 *  D-Bus++ - C++ bindings for D-Bus
 *
 *  Copyright (C) 2005-2009  Paolo Durante <shackan@gmail.com>
 *
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */


#ifndef __DBUSXX_CONNECTION_H
#define __DBUSXX_CONNECTION_H

#include <list>

#include "api.h"
#include "types.h"
#include "util.h"
#include "message.h"
#include "pendingcall.h"

namespace DBus {

class Connection;

typedef Slot<bool, const Message &> MessageSlot;

typedef std::list<Connection> ConnectionList;

class ObjectAdaptor;
class ObjectProxy;
class Dispatcher;

class DBUSXXAPI Connection
{
public:

	static Connection system_bus(Dispatcher *dispatcher = NULL);
	static Connection system_bus_private(Dispatcher *dispatcher = NULL);

	static Connection session_bus(Dispatcher *dispatcher = NULL);
	static Connection session_bus_private(Dispatcher *dispatcher = NULL);

	static Connection activation_bus(Dispatcher *dispatcher = NULL);
	static Connection activation_bus_private(Dispatcher *dispatcher = NULL);

	struct Private;

	typedef std::list<Private *> PrivatePList;

	Connection();

	Connection(RefPtrI<Private>);

	Connection(const char *address, bool priv, Dispatcher *dispatcher = NULL);

	Connection(const Connection &c);

	virtual ~Connection();

	bool operator == (const Connection &) const;

	Connection &operator = (const Connection &);

	void add_match(const char *rule);

	void remove_match(const char *rule);

	bool add_filter(MessageSlot &);

	void remove_filter(MessageSlot &);

	bool unique_name(const char *n);

	const char *unique_name() const;

	bool register_bus();

	bool connected() const;

	void disconnect();

	void exit_on_disconnect(bool exit);

	void flush();

	bool send(const Message &, unsigned int *serial = NULL);

	Message send_blocking(Message &msg, int timeout = -1);

	PendingCall send_async(Message &msg, ObjectProxy *proxy, const PendingCallSlot &slot, void* data, int timeout);

	void request_name(const char *name, int flags = 0);

	bool has_name(const char *name);

	bool start_service(const char *name, unsigned long flags);

	const std::vector<std::string>& names();

private:

	DBUSXXAPILOCAL void init();

private:

	RefPtrI<Private> _pvt;

friend class ObjectAdaptor; // needed in order to register object paths for a connection
};

class DBUSXXAPI CallInfo
{
public:

   CallInfo();
	CallInfo(const Connection &c, const Message &m);
	CallInfo(const Connection &c, const PendingCall &pc);

	const Connection &conn() const { return _conn; }
	const Message &msg() const { return _msg; }
	const Error &error() const { return _error; }

	uint32_t sender_pid();
	uint32_t sender_uid();

	CallInfo &operator = (const CallInfo& info);

protected:

	Connection _conn;
	Message _msg;
	Error _error;
};

} /* namespace DBus */

#endif//__DBUSXX_CONNECTION_H
