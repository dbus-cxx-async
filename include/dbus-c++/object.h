/*
 *
 *  D-Bus++ - C++ bindings for D-Bus
 *
 *  Copyright (C) 2005-2009  Paolo Durante <shackan@gmail.com>
 *
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */


#ifndef __DBUSXX_OBJECT_H
#define __DBUSXX_OBJECT_H

#include <string>
#include <list>

#include "api.h"
#include "interface.h"
#include "connection.h"
#include "message.h"
#include "types.h"

namespace DBus {

class DBUSXXAPI Object
{
protected:

	Object(Connection &conn, const Path &path, const char *service);

public:

	virtual ~Object();

	const DBus::Path &path() const { return _path; }

	const std::string &service() const { return _service; }

 	Connection &conn() { return _conn; }

private:

	DBUSXXAPILOCAL virtual bool handle_message(const Message &) = 0;
	DBUSXXAPILOCAL virtual void register_obj() = 0;
	DBUSXXAPILOCAL virtual void unregister_obj() = 0;

private:

	Connection	_conn;
	DBus::Path	_path;
	std::string	_service;
};

/*
*/

class ObjectAdaptor;

typedef std::list<ObjectAdaptor *> ObjectAdaptorPList;
typedef std::list<std::string> ObjectPathList;

class DBUSXXAPI ObjectAdaptor : public Object, public virtual AdaptorBase
{
public:

	static ObjectAdaptor *from_path(const Path &path);

	static ObjectAdaptorPList from_path_prefix(const std::string &prefix);

	static ObjectPathList child_nodes_from_prefix(const std::string &prefix);

	struct Private;

	ObjectAdaptor(Connection &conn, const Path &path);

	~ObjectAdaptor();

	ObjectAdaptor &object() { return *this; }

private:

	void _emit_signal(SignalMessage &);

	bool handle_message(const Message &);

	void register_obj();
	void unregister_obj();

friend struct Private;
};

/*
*/

class ObjectProxy;

typedef std::list<ObjectProxy *> ObjectProxyPList;

class DBUSXXAPI ObjectProxy : public Object, public virtual ProxyBase,
                              virtual public CallbackTarget
{
public:

	ObjectProxy(Connection &conn, const Path &path, const char *service = "");

	~ObjectProxy();

	ObjectProxy &object() { return *this; }

private:

	Message _invoke_method(CallMessage &, int timeout);
	PendingCall _invoke_method_async(CallMessage &, const PendingCallSlot &, void*, int timeout);

	bool handle_message(const Message &);

	void register_obj();
	void unregister_obj();

private:

	MessageSlot _filtered;
	PendingCalls _pending;

friend class PendingCall; //TODO: in order to manage pending calls lifetime
friend struct PendingCall::Private; //TODO:  in order to manage pending calls lifetime
};

} /* namespace DBus */

#endif//__DBUSXX_OBJECT_H
