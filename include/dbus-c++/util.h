/*
 *
 *  D-Bus++ - C++ bindings for D-Bus
 *
 *  Copyright (C) 2005-2009  Paolo Durante <shackan@gmail.com>
 *
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */


#ifndef __DBUSXX_UTIL_H
#define __DBUSXX_UTIL_H

#include <pthread.h>

#ifdef __QNXNTO__
#include <atomic.h>
#else
#include <ext/atomicity.h>
#endif

#include <set>
#include "api.h"
#include "debug.h"

namespace DBus {

//
// Platform independent atomic support
//
#ifdef __QNXNTO__

class AtomicCounter
{
public:
	typedef unsigned tAtomicCnt;

	AtomicCounter(tAtomicCnt init_val):
	   _counter(init_val)
	{}

	operator tAtomicCnt()
	{
	   return atomic_add_value(&_counter, 0U);
	}

   void inc()
   {
      atomic_add(&_counter, 1);
   }

   void dec()
   {
      atomic_sub(&_counter, 1);
   }

protected:
   volatile tAtomicCnt _counter;
};

#elif defined(__GNUC__)

class AtomicCounter
{
public:
   typedef _Atomic_word tAtomicCnt;

   AtomicCounter(tAtomicCnt init_val):
      _counter(init_val)
   {}

   operator tAtomicCnt()
   {
      return __gnu_cxx::__exchange_and_add(&_counter, 0);
   }

   void inc()
   {
      __gnu_cxx::__atomic_add(&_counter, +1);
   }

   void dec()
   {
      __gnu_cxx::__atomic_add(&_counter, -1);
   }

protected:
   volatile tAtomicCnt _counter;
};

#else
#error Atomic operations are not defined for this OS and/or compiler
#endif

/*
 *   Very simple reference counting
 */

class DBUSXXAPI RefCnt
{
public:

	RefCnt()
	{
		__ref = new AtomicCounter(1);
	}

	RefCnt(const RefCnt &rc)
	{
		__ref = rc.__ref;
		ref();
	}

	virtual ~RefCnt()
	{
		unref();
	}

	RefCnt &operator = (const RefCnt &ref)
	{
		if (&ref != this)
		{
			ref.ref();
			unref();
			__ref = ref.__ref;
		}
		return *this;
	}

	bool noref() const
	{
	   return (*__ref) == 0;
	}

	bool one() const
	{
		return (*__ref) == 1;
	}

private:

	DBUSXXAPILOCAL void ref() const
	{
		__ref->inc();
	}

	DBUSXXAPILOCAL void unref()
	{
		__ref->dec();

		if ((*__ref) < 0)
		{
			debug_log("%p: refcount dropped below zero!", __ref);
		}

		if (noref())
		{
			delete __ref;
			__ref = 0;
		}
	}

private:

   AtomicCounter* __ref;
};

/*
 *  Pthreads-based mutual exclusion
 */
class Synchronized
{
protected:

	struct Guard
	{
		Guard(const Synchronized *s) : _s(s)
		{
			_s->lock();
		}
		~Guard()
		{
			_s->unlock();
		}
		const Synchronized *_s;
	};

	void lock() const
	{
		pthread_mutex_lock(&__mtx);
	}

	void unlock() const
	{
		pthread_mutex_unlock(&__mtx);
	}

protected:

	Synchronized()
	{
	   pthread_mutex_init(&__mtx, NULL);
	}

public:

	~Synchronized()
	{
		pthread_mutex_destroy(&__mtx);
	}

private:

   // Not supported - unimplemented
   Synchronized(const Synchronized& lhs);
   Synchronized& operator=(const Synchronized& ref);

	mutable pthread_mutex_t __mtx;
};

/*
 *   Reference counting pointers (emulate boost::shared_ptr)
 */

class RefPtrVoid
{
public:

   RefPtrVoid(void *ptr)
   : __ptr(ptr)
   , __cnt()
   {}

   RefPtrVoid(const RefPtrVoid& lhs)
   : __ptr(0)
   , __cnt()
   {}

	virtual ~RefPtrVoid()
	{}

	bool operator == (const RefPtrVoid &ref) const
	{
		return __ptr == ref.__ptr;
	}

protected:

	void *__ptr;
	RefCnt __cnt;
};

template <class T>
class RefPtrI : public RefPtrVoid		// RefPtr to incomplete type
{
public:

	RefPtrI(T *ptr = 0);

   RefPtrI(const RefPtrI<T>& lhs);

	~RefPtrI();

	RefPtrI<T>& operator = (const RefPtrI<T> &ref);

	T &operator *() const
	{
		return *get();
	}

	T *operator ->() const
	{
		return get();
	}

	T *get() const
	{
		return static_cast<T *>(__ptr);
	}
};

template <class T>
class RefPtr : public RefPtrVoid
{
public:

	RefPtr(T *ptr = 0)
	: RefPtrVoid(ptr)
	{}

   RefPtr(const RefPtr<T>& lhs)
   : RefPtrVoid(0)
   {
      operator=(lhs);
   }

	~RefPtr()
	{
      if (__cnt.one()) delete get();
	}

	RefPtr<T>& operator = (const RefPtr<T>& ref)
	{
      if (this != &ref)
      {
         // If there is only a single reference (that's us) then ...
         if (__cnt.one())
         {
            delete get();
            __ptr = 0;
         }
         __ptr = ref.__ptr;
         __cnt = ref.__cnt;
      }
      return *this;
	}

	T &operator *() const
	{
		return *get();
	}

	T *operator ->() const
	{
		return get();
	}

	T *get() const
	{
		return static_cast<T *>(__ptr);
	}
};

/*
 *   Typed callback template
 */

//
// Forward Declarations
//
class CallbackEmitter;
class CallbackTarget;

class CallbackEmitter : protected Synchronized
{
public:
   CallbackEmitter();
   virtual ~CallbackEmitter();

protected:
   void connect(CallbackTarget* tgt);

   void disconnect();

   bool isConnected() const;

private:
   CallbackTarget* target;
   friend class CallbackTarget;
};

class CallbackTarget : protected Synchronized
{
public:
   CallbackTarget();
   virtual ~CallbackTarget();

private:
   void disconnect_emitter(CallbackEmitter* cbEmitter);
   void connect_emitter(CallbackEmitter* cbEmitter);

   typedef std::set<CallbackEmitter*> EmitterCollection;
   EmitterCollection emitters;
   friend class CallbackEmitter;
};

//
// CallbackTarget Implementation
//
inline CallbackTarget::CallbackTarget()
   : Synchronized()
   , emitters()
{
   debug_log("CallbackTarget(%p)::Constructed");
}

inline CallbackTarget::~CallbackTarget()
{
   Guard g(this);
   EmitterCollection::const_iterator it;
   // Tell all the callback emitters that this target is
   // being disconnected.
   debug_log("CallbackTarget(%p)::# Emitters to disconnect: %u",
            this, emitters.size());

   for ( it = emitters.begin(); it != emitters.end(); ++it )
   {
      debug_log("CallbackTarget(%p)::Disconnect emitter: %p", this, *it);
      (*it)->disconnect();
   }
   emitters.clear();
   debug_log("CallbackTarget(%p)::Destroyed", this);
}

inline void CallbackTarget::disconnect_emitter(CallbackEmitter* cbEmitter)
{
   Guard g(this);
   debug_log("CallbackTarget(%p)::Erasing emitter: %p", this, cbEmitter);
   emitters.erase(cbEmitter);
}

inline void CallbackTarget::connect_emitter(CallbackEmitter* cbEmitter)
{
   Guard g(this);
   debug_log("CallbackTarget(%p)::Inserting emitter: %p", this, cbEmitter);
   emitters.insert(cbEmitter);
}

//
// CallbackEmitter Implementation
//

inline CallbackEmitter::CallbackEmitter()
   : Synchronized()
   , target(0)
{
   debug_log("CallbackEmitter(%p)::Constructed", this);
}

inline CallbackEmitter::~CallbackEmitter()
{
   Guard g(this);

   if ( target )
   {
      debug_log("CallbackEmitter(%p)::Disconnecting from target: %p", this, target);
      target->disconnect_emitter(this);
   }

   debug_log("CallbackEmitter(%p)::Destroyed", this);
}

inline void CallbackEmitter::connect(CallbackTarget* tgt)
{
   Guard g(this);

   if ( target && (target != tgt) )
   {
      // Unregister from the (old) target first
      debug_log("CallbackEmitter(%p)::Unregistering emitter from target: %p", this, target);
      target->disconnect_emitter(this);
   }

   target = tgt;
   if ( target )
   {
      debug_log("CallbackEmitter(%p)::Connecting to target: %p", this, target);
      target->connect_emitter(this);
   }
}

inline void CallbackEmitter::disconnect()
{
   Guard g(this);
   debug_log("CallbackEmitter(%p)::Disconnecting target: %p", this, target);
   target = 0;
}

inline bool CallbackEmitter::isConnected() const
{
   return 0 != target;
}


template <class R, class P>
class Callback_Base : public CallbackEmitter
{
public:
   Callback_Base() : CallbackEmitter() {}

	virtual R call(P param) const = 0;

	virtual ~Callback_Base()
	{}
};

template <class R, class P>
class Slot
{
public:

	Slot &operator = (Callback_Base<R,P>* s)
	{
		_cb = s;

		return *this;
	}

	R operator()(P param) const
	{
		/*if (_cb.get())*/ return _cb->call(param);
	}

	R call(P param) const
	{
		/*if (_cb.get())*/ return _cb->call(param);
	}

	bool empty()
	{
		return _cb.get() == 0;
	}

private:

	RefPtr< Callback_Base<R,P> > _cb;
};


template <class C, class P>
class CallbackNoReturn : public Callback_Base<void,P>
{
public:

	typedef void (C::*M)(P);

	CallbackNoReturn(C *c, M m)
	: _c(c), _m(m)
	{
	   debug_log("CallbackNoReturn(%p)::Ctor - connect to target: %p", this, c);
	   CallbackEmitter::connect(c);
	}

	virtual ~CallbackNoReturn()
	{
	   debug_log("CallbackNoReturn(%p)::Destroyed", this);
	}

   void call(P param) const
   {
      Synchronized::Guard g(this);

      if ( CallbackEmitter::isConnected() )
      {
         (_c->*_m)(param);
      }
      return;
   }

private:

	C *_c; M _m;
};

template <class C, class R, class P>
class Callback : public Callback_Base<R,P>
{
public:

   typedef R (C::*M)(P);

   Callback(C *c, M m, const R& def)
   : _c(c), _m(m), _d(def)
   {
      debug_log("Callback(%p)::Ctor - connect to target: %p", this, c);
      CallbackEmitter::connect(c);
   }

   virtual ~Callback()
   {
      debug_log("Callback(%p)::Destroyed", this);
   }

   R call(P param) const
   {
      Synchronized::Guard g(this);

      if ( CallbackEmitter::isConnected() )
      {
         return (_c->*_m)(param);
      }
      return _d;
   }

private:

   C *_c; M _m; R _d;
};

} /* namespace DBus */

#endif//__DBUSXX_UTIL_H
